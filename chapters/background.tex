\chapter{Background and Concepts}
\label{ch:background}
\

Before presenting \archname, some important background and
concepts that form the basis of the architecture are presented
and discussed in this chapter. Namely, a brief introduction to
Service Function Chaining (SFC) including key IETF standards
(\S\ref{sec:sfc}), an original taxonomy of service function
implementations (\S\ref{sec:sfc-app-types}), details on Berkeley
Packet Filter (BPF) technology (\S\ref{sec:bpf}), an overview of
the layers in the Linux networking stack
(\S\ref{sec:linux-stack}), and a brief introduction to container
networking (\S\ref{sec:containers}).

\section{Service Function Chaining (SFC)} \label{sec:sfc}

Network Function Virtualization (NFV) is a topic that has
received much attention from both academy and industry
researchers in recent years. At its core, it consists on an
approach to migrate applications that were traditionally
implemented in hardware to virtualized infrastructures, running
in commodity off-the-shelf devices. This decreases acquisition
and management costs, while providing more flexibility and
scalability \cite{Mijumbi2016}.

Network functions can be combined into chains, an ordered list of
functions to be executed sequentially, providing composed
services. Examples of such functions include firewalls, load
balancers, and deep packet inspectors. This interconnection of
functions is called Service Function Chaining (SFC).

Traditionally, SFC implementations relied on complex routing
schemes to steer traffic through network functions
\cite{Quinn2014}. This made modifications to service functions
difficult, usually requiring significant changes in network
configuration, which is error-prone and cumbersome. Because these
deployments were tightly coupled to network topology, they also
tended to be restricted to a specific Service Provider (SP)
domain, hardly being applicable to different scenarios. Besides,
different third-party service functions have a low level of
interoperability.

SFC has gained momentum in recent years
\cite{Xia2015,John2013,Sahhaf2015,Kitada2014,Quinn2014} causing
the Internet Engineering Task Force (IETF) to publish a problem
statement for SFC defining key areas that working groups could
investigate towards new SFC solutions, in the form of RFC 7498
\cite{rfc7498}. This document states the key challenges faced by
traditional service function chaining implementations and
establishes focus areas to guide workgroups to propose new
protocols and solutions for those issues.

\subsection{IETF Reference SFC Architecture} \label{sec:arq-rfc7665}

A product of such effort is RFC 7665 \cite{rfc7665}, a standard
that proposes a reference architecture for SFC, illustrated in
Figure \ref{fig:SFC-Domain}. In this model,  an SFC encapsulation
header is added to incoming packets, containing information about
the current chain being executed and the next hop in the
sequence. The chaining is enabled by the interaction of four
different types of elements:

\begin{itemize}
    \item \textbf{Classifiers}: packets ingressing the SFC
    environment are classified to determine which chaining should
    be executed for them, following the insertion of the
    corresponding SFC encapsulation;

    \item \textbf{Service Functions}: execute some kind of
    processing over the packet. They can be divided into two
    categories: \textit{SFC-aware} and \textit{SFC-unaware}. The
    first is composed of functions that have knowledge about the
    SFC encapsulation and know how to operate on it. The second,
    also called legacy functions, are those not aware of the
    protocol, needing the cooperation of a Proxy to be part of
    the SFC environment;

    \item \textbf{Proxies}: are responsible for implementing an
    interface between SFC unaware functions and the SFC domain.
    They remove and re-insert the SFC encapsulation before and
    after the service function has executed, respectively;

    \item \textbf{Forwarders}: at each step in the chain, a
    forwarder is responsible for determining which is the next
    service to be executed in the chain. This decision is based
    on an SFC forwarding table configured by the administrator
    and on the fields of the packet's SFC encapsulation. The
    forwarder is also responsible for changing the external
    transport encapsulation to ensure the underlay can deliver
    the packet to the next function in the chain.
\end{itemize}

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\columnwidth]{./figs/SFC-Domain.png}
	\caption{Illustration of RFC 7665 reference architecture. Adapted from \cite{rfc7665}.}
	\label{fig:SFC-Domain}
\end{figure}

As shown in Fig \ref{fig:SFC-Domain}, packets are classified upon
arrival to the network by the Classifier. In this step a packet
is matched against a set of pre-configured rules and a service
path is chosen, if one exists for that packet. The SFC
encapsulation containing the corresponding path information is
added and the packet is sent to the next element in the
architecture: the Forwarder. This element decides which SF should
be executed next and alters the packet's external transport
encapsulation accordingly, letting the network route it to the
next hop or to its corresponding proxy, in case of an SFC-unaware
function.

In that case, the Proxy will remove the SFC encapsulation and
then forward the packet to the SF. After performing any
operations on it, the SF will send the packet back to the Proxy
for reinsertion and update of the SFC encapsulation, sending it
back to the Forwarder afterwards. This process is repeated until
the last SF in the chain has executed. That is when the Forwarder
finally removes the SFC encapsulation and lets the packet follow
its regular flow in the network.

\subsection{Network Service Header (NSH)} \label{sec:nsh}

RFC 8300 \cite{rfc8300} complements the architecture presented
above with the specification of the Network Service Header (NSH),
an encapsulation protocol to be used by SFC deployments.
Combined, these standards are capable of providing service
chaining in practice.

The NSH is independent of the outter transport encapsulation, but
needs it to move the packets around the network. It provides
service chaining as defined by RFC 7665 by adding an extra header
to the packet. This new header is added between the outter
transport encapsulation and the packet itself, as illustrated by
Figure \ref{fig:nsh-encap}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{figs/nsh-header.pdf}
	\caption{Packet encapsulated with NSH}
	\label{fig:nsh-encap}
\end{figure}

The NSH header is divided into three parts:

\begin{itemize}
	\item{Base Header (4 Bytes)}: Provides general information
	like protocol version, next protocol, time to live (TTL),
	header length and MD Type, which determines the kind of
	metadata being carried.
 	\item{Service Path Header (4 Bytes)}: Holds path
 	identification and current position within the service path.
	\item{Context Header (Variable Length)}: Carries metadata
	to be shared between SFs.
\end{itemize}

A more detailed explanation of each field in the Base Header, the
different MD Types available and metadata definition are out of
the scope of this document. For such, please refer to the text of
RFC 8300. Only MD Type 1 NSH was used throughout this work, which
consists on four 4-byte fixed fields, 16 Bytes in total.

The Service Path Header (SPH) is a 4-byte field that consists of
two subfields: a 3-byte Service Path Identifier (SPI) and an
1-byte Service Index (SI). The SPI represents an unique
identifier for a service function path. This value is first set
by the Classifier and is used by the other elements to know
which service path is being executed on the packet. The SI is an
index indicating the current location in the service function
graph. The Classifier initially sets this value to 255,
indicating the beginning of a path.
%  This initial value may also
% be configured to match service path length.

Throughout service path execution, if there is no packet
reclassification, the SPI remains the same until the end.
However, the SI is decremented by one after being processed by
each service function. For SFC-aware SFs, this is done by the
function itself, while for the SFC-unaware this is left to the
Proxy.

To make the decision of what is the next hop for a given node in
the graph, the Forwarder needs to maintain a lookup table with
the SPI/SI pair mappings to service functions reachable from it.
This table may also contain their respective addressing, along
with the transport encapsulation to be used. A partial
representation of a possible implementation of such table is
illustrated in Table \ref{tbl:Forwarder Table}:

\begin{table}[h]
	\centering
	\caption{Example of Forwarder's lookup table. Adapted from \cite{rfc8300}.}
	\label{tbl:Forwarder Table}
	\begin{tabular}{ | c | c | c | c |  }
	\hline
	SPI & SI  & Next hop           & Transport Encapsulation \\ \hline
	10  & 255 & 192.0.2.1          & VXLAN-gpe               \\
	10  & 254 & 198.51.100.10      & GRE                     \\
	10  & 251 & 198.51.100.15      & GRE                     \\
	40  & 251 & 198.51.100.15      & GRE                     \\
	50  & 200 & 01:23:45:67:89:ab  & Ethernet                \\
	\vdots & \vdots & \vdots & \vdots						 \\
	10  & 212 & Null (end of path) & None 					 \\
	\hline
	\end{tabular}
\end{table}

Using the table above as an example, suppose the Forwarder
receives a packet with SPI=10 and SI=255. The Forwarder will
check its table and know the next hop is at IP 192.0.2.1 through
VXLAN-gpe. Once the packet is processed by that node it will have
its SI value decremented and will be sent back to the Forwarder,
which in turn will check the table again. This process is
repeated for every service function in the chain, until the SI
becomes 212. In this case the packet has reached the end of the
path, indicated by the Null entry in the table. The packet will
then be decapsulated and sent back to the network.


\section{Service Function Types} \label{sec:sfc-app-types}

The implementation of software service functions can be done in
different ways, which alter the way they interface with the
underlying operating system for packet processing. An original
taxonomy is provided below in order to distinguish the different
natures of implementations available and to clarify how
\archname~handles each one. The following types have been
identified as being the most prominent:

\subsection{\textbf{Kernel-supported (KS)}}

These are functions implemented as common processes running in
user space. They rely on standard mechanisms to receive packets
from the network, such as system calls. This kind of
implementation heavily relies on the facilities offered by the
underlying operating system, which by default implements a
complex network stack to handle multiple protocol types. However,
with time this form of implementation has struggled to handle the
ever-growing traffic rates supported by modern network cards. The
penalty imposed by system interruptions and the constant switch
between user and kernel modes for packet handling has hindered
the use of this kind of applications for fast packet processing
.%\red{REF}.

\subsection{\textbf{Kernel-bypass (KB)}}

As the name implies, applications implemented with this technique
perform a complete bypass of the kernel stack and implement all
packet processing functionality in user space. They usually make
use of specialized drivers in polling mode so packets can be
shared directly between network devices and user processes while
avoiding additional delays caused by the traditional packet
handling model based on interruptions and system calls. As a side
effect, they have little to no dependence of the network
facilities offered by the kernel stack, so all needed packet
parsing and protocol handling has to be re-implemented in user
space. Good examples of applications using this approach are
those based on libraries such as DPDK \cite{DPDK2020} and NetMap
\cite{Luigi2020}.


\subsection{\textbf{Kernel-only (KO)}}

This class of applications aims to offer superior performance by
using the opposite approach of KB programs: residing entirely in
kernel space. Since one of the biggest performance bottlenecks in
packet processing is the constant crossing of the barrier between
kernel and user spaces, kernel-only functions implement all their
functionality inside the kernel.

Classic examples are Click-based applications
\cite{kohler2000click} as well as packet processing leveraging
\textit{iptables}, both of which rely on the use of kernel
modules, but have not been able to keep up with higher packet
rates. KO functions have been gaining more traction in recent
years \cite{miano2018} with the latest improvements to the BPF mechanism inside
the Linux kernel (\S\ref{sec:bpf}) and new technologies such as
XDP (\S\ref{ssec:xdp}). These new technologies allow generic
user-specified programs to be loaded to the kernel on run time,
effectively allowing them to alter the kernel's network stack
functionality in a dynamic manner.


\section{Berkeley Packet Filter (BPF)} \label{sec:bpf}

\subsection{History}

The Berkeley Packet Filter (BPF)
\cite{McCanne:1993:BPF:1267303.1267305} was initially proposed by
Steven McCanne and Van Jacobson in 1992 as a solution to perform
packet filtering on the kernel of Unix BSD systems. It consisted
of an instruction set and a virtual machine (VM) for executing
programs written in that language.
% Until today it is a core
% component of \textit{libpcap}, a library widely used by network
% applications such as \textit{tcpdump} and Wireshark
% \cite{orebaugh2006wireshark}.

The bytecode of a BPF application was transferred from user space
to the kernel, where it was checked to assure security and
prevent kernel crashes. After passing the verification, the
system attached the program to a socket with an associated BPF VM
and ran it on each arriving packet. The Linux kernel has
supported BPF since version 2.5, with no major changes to the BPF
core code until 2011, when besides the interpreter, the kernel
gained support for a dynamic BPF translator~\cite{JIT2011}. Instead
of interpreting the BPF byte code, the kernel was now able to
translate BPF programs directly into x86 instructions.

One of the most prominent tools that use BPF is the
\texttt{libpcap} library, used by the \texttt{tcpdump} tool. When
using \texttt{tcpdump} to capture packets, a user can set a
packet filtering expression so that only packets matching that
expression are actually captured. For example, the expression
"\texttt{ip and tcp}" captures all IPv4 packets that contain the
TCP transport layer protocol. This expression can be reduced by a
compiler to BPF bytecode.

\subsection{Extended Berkeley Packet Filter (eBPF)}

In 2013, BPF received a major upgrade with a whole set of new
functionalities\footnote{https://lwn.net/Articles/740157/}. This
new proposal was made by Alexei Starovoitov and was coined
\textit{extended Berkeley Packet Filter} (eBPF). With time, eBPF
became the new standard and inherited the name of its
predecessor, usually being referred to as BPF, with the previous
version now being called \textit{classic BPF} (cBPF). On the
remaining sections in this text, the term BPF refers to the
improved version used today (eBPF).

The differences between cBPF and eBPF are considerable, starting
from the number of registers available, which increased from 2 to
11. Programs can now be chained in sequence using \textit{tail
calls}, and they have access to a whole new framework of helper
functions and tools to interact with user space and many
subsystems inside the Linux kernel. However, one of the most
important is the addition of maps: key-value structures used to
store data between program executions that can also be used to
exchange data between programs and process in user space, for
example. The kernel currently provides 20+ different types of
maps, ranging from hash tables, arrays, stacks, queues,
redirection tables, among others.

These new features greatly improved the applicability of BPF for
the implementation of generic packet processing and tracing
inside the kernel. This is proven by the growing adoption by
major companies such as Facebook, Cloudflare, and Netronome
\cite{Katran2018, bertin2017xdp, Netronome2018}, which have
already used BPF to implement network monitoring, network traffic
manipulation, load balancing, and system profiling.

\subsection{BPF system}

The BPF system is composed of a series of components to compile,
verify, and execute the source code of developed applications.
The typical workflow of the BPF system is illustrated in
Figure~\ref{fig:bpf-kernel}. It is written in a high-level
language, mainly a subset of C, and compiled\footnote{Both
\texttt{LLVM} and \texttt{GCC} provide backends for BPF, but
\texttt{LLVM} currently has better support as it was the compiler
infrastructure adopted by the kernel community to implement most
BPF-related features.} to an ELF/object code containing the BPF
instructions.

This file is then passed to a loader that can then
insert it into the kernel using a special system call. During
this process, the verifier analyzes the program and upon approval
the kernel performs the dynamic translation (JIT). Besides being
executed by the processor, the program can also be offloaded to
specialized hardware.

\begin{figure}[h]
    \includegraphics[width=0.9\textwidth]{./figs/bpf-and-kernel.pdf}
    \centering
	\caption{Typical workflow of loading BPF programs to the kernel.}
	\label{fig:bpf-kernel}
\end{figure}

To ensure the integrity and security of the operating system, the
kernel uses a verifier that performs static program analysis of
BPF instructions being loaded into the system. Among other
things, it checks if a program is larger than maximum limit
allowed (current limit is $10^6$ instructions), whether or not
the program terminates, if the memory accesses are within the
memory range allowed for the program, and how deep the execution
path is. It is called after the code has been compiled and during
the process of loading the program into the data plane
\cite{miller2019verifier}.

\section{Linux Networking Stack} \label{sec:linux-stack}

On Linux, packets entering the OS are processed by several layers in the
kernel, as shown in Figure~\ref{fig:linux-kernel-stack}. They can
be roughly divided in: socket layer, TCP stack, Netfilter,
Traffic Control (TC), the eXpress Data Path (XDP), and the NIC.

\begin{figure}[htpb]
\centering
\includegraphics[width=.7\textwidth]{figs/linux-kernel-stack.pdf}
\caption{Linux kernel network stack.}
\label{fig:linux-kernel-stack}
\end{figure}

Packets destined to a standard user space application go through
all these layers and can be intercepted and modified during this
process by modules such as \texttt{iptables}, which resides in
the Netfilter layer. As explained before, BPF programs can be
attached to several places inside the kernel, enabling packet
mangling and filtering.

% Códigos BPF podem ser carregados em diversos ganchos
% (\textit{hook points}) no kernel, que é composto por diversas
% camada de processamento de pacotes, como mostra a Figura
% \ref{fig:linux-kernel-stack}.

BPF programs attached to each layer see different contexts, i.e.
the input data passed to them, and have distinct sets of helper
functions available for use. For example, programs in the XDP
layer are the first to interact to incoming packets on RX and and
received a Layer-2 frame as its context. Moreover, they can call
specific helper functions only available in this layer, for
altering a packet's size, for example. Programs on the Netfilter
layer, for example, see a Layer-3 packet as their context, not
being able to alter the Ethernet header, but they have extra
helper functions related to routing, checksum calculation and
tunneling, not available on the XDP. In the following subsections
the two main layers used in this work are described: XDP
(\S\ref{ssec:xdp}) and TC (\S\ref{ssec:tc}).

\subsection{eXpress Data Path (XDP)} \label{ssec:xdp}

XDP is the lowest layer of the Linux kernel network stack. It is
present only on the RX path, inside a device's network driver,
allowing packet processing at the earliest point in the network
stack, even before memory allocation is done by the OS. It
exposes a hook to which BPF programs can be attached and executed
for every received packet
\cite{Hoiland-Jorgensen:2018:EDP:3281411.3281443}.

In this hook, programs are capable of taking quick decisions
about incoming packets and also performing arbitrary
modifications on them, avoiding additional overhead imposed by
processing inside the kernel. This renders the XDP as the best
hook in terms of performance speed for applications such as
mitigation of DDoS attacks.

After processing a packet, an XDP program returns an action, a
value that represents the final verdict regarding what should be
done to the packet after program exit. The possible actions
include dropping, passing it along the stack, sending it back to
the network or forwarding it to another interface.

XDP is designed for fast packet processing applications while
also improving programmability. In addition, it is possible to
add or modify these programs without modifying the kernel source
code, just like with other BPF hooks. For extra performance,
programs can also be offloaded to compatible SmartNICs to be
executed in hardware.

This layer can also be used to add programmability to
kernel-bypass applications that use AF\_XDP sockets
\footnote{https://lwn.net/Articles/750293/}. This special socket
family was designed to provide the benefits of the XDP to
applications that do not make use of the kernel's network
facilities, but do all its execution in user space, such as those
based on the DPDK set of libraries. With them,
XDP programs are executed before packets are sent to user space,
combining the the programmability offered by XDP with the extra
performance provided by kernel-bypassing.

\subsection{Traffic Control (TC) Hook} \label{ssec:tc}

Although the XDP layer is well suited for many interesting
applications, it can only process ingress traffic. The closest
layer to the NIC on egress is the Traffic Control (TC) layer,
also available on ingress. It is responsible for executing
traffic control policies on Linux. In it, network administrators
can configure different queuing disciplines (\textit{qdisc}) for
the various packet queues present in the system, as well as add
filters to deny or modify packets.

The TC has a special queuing discipline type called
\texttt{clsact} that exposes a hook that allows queue processing
actions to be defined by BPF programs. They receive pointers to
Ethernet frames like on XDP, but when a packet reaches the TC
on ingress, the kernel has already parsed its headers to extract
protocol metadata, hence richer context information is passed to
the BPF programs attached to it. Extra helper functions are also
available for programs on this layer compared to XDP.

\section{Container networking} \label{sec:containers}

Containers are being widely used today to deploy cloud
applications aa they provide a lightweight and reproducible
environment that can be used both during development as in
production. Tools like Docker \cite{merkel2014docker} and
Kubernetes \cite{burns2016borg} have become commonplace and are
the de facto standard for application deployment. Although a
deeper discussion about container platforms is out of the scope
if this text, the way Docker manages container networking is
briefly discussed, as it was the main container technology used
the main container technology used in this work.

A container is a process supported by several additional
mechanisms to provide isolation between instances.
% \red{REF}.
These aim to guarantee separation in terms of networking, file
system management, memory access, and so on. When it comes to
networking, Docker provides isolation by using
\textit{namespaces} on Linux. Each namespace represents a
separate network environment with its own set of interfaces,
routing tables and configuration, and usually each container has
its own network namespace. Although several ways to provide
connectivity between containers are supported by
Docker~\cite{marmol2015networking} one of the most common is a
combination of \textit{veth pairs} and bridges.

Veth (virtual Ethernet) interfaces are always created in pairs
and represent a direct link implemented in software: every packet
transmitted on one side will be received by the other. The
\texttt{veth} interface type received support for Native XDP on
kernel 4.14 \cite{veth-xdp}. This kind of device can be used to
create a direct connection between two containers or, more
commonly, to create a link between the container and its host.
This is done by attaching one veth interface on the container's
network namespace and the other on the host's namespace. However,
it is usually necessary to provide communication between
containers as well, in which case a bridge or virtual switch is
used. In this case, the end of the pair given to the host is
connected to a virtual switch, like a Linux bridge or Open
vSwitch (OVS) \cite{pfaff2015design}, which forwards packets
between containers.