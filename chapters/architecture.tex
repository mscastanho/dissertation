\chapter{Chaining-Box} \label{ch:architecture}

As discussed before, existing SFC architectures are usually
dependent on specialized devices or limited to specific
environments, making them tailor-made for a single environment
and set of technologies. In order to create a more generic
chaining architecture, one that is not coupled to the network and
can be applied to different scenarios, it is necessary to move
all SFC functionality from the standard data plane to a separate
service plane. This idea resembles more closely what was
initially envisioned by RFC 7665 and removes the need for
underlay network devices to have knowledge about the SFC
protocols and mechanisms in use.

This chapter presents \archname, an SFC architecture that aims to
provide chaining functionality in a transparent manner, requiring
little cooperation from the underlay network besides forwarding
packets based on standard Internet protocols. In the following
sections, its data (\S\ref{sec:dataplane}) and control planes
(\S\ref{sec:controlplane}) are discussed, followed by
an architecture overview (\S\ref{sec:architecture}) and a
comparison with other approaches (\S\ref{sec:arch-comparison}).

\section{Data Plane} \label{sec:dataplane}

% Like RFC 7665, Cadeia-Aberta uses the NSH as its SFC
% encapsulation protocol. The key difference, however, is that
% all SFC operations are split into four processing stages
% incorporated at each SF:

% Packets are processed in order by each of the stages above,
% before being sent back to the network. They are then forwarded
% by the network to the next function in the chain based on the
% outter transport encapsulation. Since all SFC functionality is
% placed inside each SF, there is no need for intermediate
% steering elements to act on the NSH, so SFs can communicate
% directly with each other, as shown in Figure
% \ref{fig:chaining-arq}. This presents many advantages, as
% explained in Section~\ref{sec:pros-cons}.

Inspired by RFC 7665, \archname~uses an extra SFC encapsulation
to provide the chaining, namely the NSH. The operations over this
header are limited to only three kinds: classification,
insertion/removal, and forwarding of packets between nodes in a
chain. To fully decouple the service plane from the data plane,
\archname~uses a simple approach: incorporate all SFC-related
actions into the execution unit of each service function, i.e.
virtual machines or containers.

% These elements should operate over an SFC encapsulation, which
% is a protocol to enable service function path execution.
% PhantomSFC uses NSH, standardized by RFC 8300 \cite{rfc8300},
% as its SFC encapsulation protocol. NSH has been chosen for
% three main reasons: (i) it is compliant with RFC 7665 and
% independent of the transport encapsulation used; (ii) it allows
% metadata sharing between SFC elements, which is important to
% carry service management and configuration information; and
% (iii) being an RFC, it's likely that it will be improved and
% more widely adopted over time.

Ideally, the addition of such features should be done without the
need to refactor or modify the source code of a service function,
being totally transparent to it. This can be done in the form of
processing stages run before and after the SF's execution, as
illustrated by Figure \ref{fig:chaining-box-sf}. Together with
the function per se, these stages form a single box fully capable
of forwarding packets between different SFs organized in chains,
hence the name of the architecture. Individually, each of these
boxes is called a \textit{\boxname}.

\begin{figure*}[ht]
    \centering
    \includegraphics[width=\textwidth]{figs/ChainingBox-SF-EN.pdf}
    \caption{Example of a \boxname~and its processing stages}
    \label{fig:chaining-box-sf}
\end{figure*}

As shown above, a \boxname~is composed of four separate
processing stages:

\begin{enumerate}
    \item \textbf{Decapsulation (\textit{Dec})}: upon reception
    of an NSH-encapsulated packet, this stage is responsible for
    removing the NSH header and storing this information
    temporarily for later header re-insertion by the
    encapsulation stage. In case of an NSH-aware function, this
    stage is not needed;

    \item \textbf{Service Function (\textit{SF})}: this stage
    represents the actual service function, and is not directly
    implemented by the architecture, but packets must be steered
    through it after the decapsulation stage;

    \item \textbf{Encapsulation (\textit{Enc})}: after the packet
    has been processed by the service function, the NSH is
    re-inserted by this stage, also decrementing the SI field. In
    case of an NSH-aware function, this stage is not needed;

    \item \textbf{Forwarding (\textit{Fwd})}: this stage makes
    forwarding decisions based on the SI and SPI indexes from the
    NSH header. It looks up an SFC forwarding table and rewrites
    the packet's transport encapsulation headers before sending
    it back to the network to be forwarded to the next hop in the
    chain.
\end{enumerate}

Dec and Enc stages implement all proxy actions, so they are only
needed when dealing with an NSH-unaware SF. For NSH-aware
functions, however, no changes on the SFC encapsulation are
necessary, and thus these stages can be deactivated, hence the
dashed lines on Figure \ref{fig:chaining-box-sf}. In this case,
packets sent to a \boxname~are directly received by the SF stage.
Since the function is agnostic of the processing stages,
\archname~supports different kinds of packet matching algorithms
to remove and re-insert the NSH, only requiring modifications to
how Dec and Enc stage are implemented.

One of the greatest benefits of local 1-to-1 forwarding stages
instead of a centralized 1-to-many forwarding element as in RFC
7665 is that it allows functions to communicate directly, without
the need of an intermediary, greatly reducing the overhead on
forwarding components and avoiding single points of failure. This
allows the architecture to assume more generic logical topologies
and a more distributed form, less centralized on individual
elements. This difference in logical topology can be observed on
Figure \ref{fig:arq-comp}.

The scenario shown is composed by four service functions. Each
block can be seen as a separate execution unit, e.g. a virtual
machine. Figure \ref{fig:chaining-box-arq} shows that on
\archname, each SF is contained in a \boxname~with all needed SFC
functionality within it, and communication between SFs in the
service plane is done directly, without a middle element. On the
other hand, RFC 7665 (Figure \ref{fig:standard-arq}) uses extra
levels of indirection, requiring that all communication goes
through a Forwarder and possibly through and additional Proxy.

\begin{figure}[ht]
    \centering
    \subfloat[][\archname]{\label{fig:chaining-box-arq} \includegraphics[width=0.7\textwidth]{./figs/chaining-box-arq.pdf}}
    \qquad
    \subfloat[][RFC 7665]{\label{fig:standard-arq} \includegraphics[width=0.57\textwidth]{./figs/standard-arq.pdf}}

    \caption{Example scenarios with different SFC architectures.}
    \label{fig:arq-comp}
\end{figure}

As all the heavy lifting is now performed by the processing
stages, there are only two tasks left for the underlying network:
support packet classification and route packets between SFs.

Note that none of the stages discussed above implement
classification, but rather expect that packets have already been
classified and encapsulated with NSH before arriving to a
\boxname. This requires cooperation from a border router, either
to classify packets when they enter the network, or to route them
to a dedicated service function implementing classification that
serves as an entry point for all ingress traffic. The first
option breaks the decoupling offered by \archname, while the
second can be achieved through simple routing rules on routers
but may suffer from bottlenecks caused by the classifier SF.
However, this can be mitigated by using well-known load balancing
techniques.

Besides that, the network is also responsible for routing the
packets between each service function. But this is done in an
agnostic way, as network devices will route packets based only on
the external transport encapsulation, be it VXLAN, Geneve, GRE or
any other tunneling scheme, while the NSH will remain
encapsulated in the tunnel.

\section{Control Plane} \label{sec:controlplane}

The same attributions of the control plane from RFC 7665 also
apply to \archname. A controller needs to setup rules on
forwarding elements to specify the chains. In \archname~terms,
the Fwd stage of each managed \boxname~needs the entries in their
forwarding tables to effectively enable the chaining. These
actions are handled by the control plane, which is divided in two
separate parts: an agent running on each \boxname~and a remote
controller, as shown in Figure \ref{fig:control-plane}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{figs/chaining-box-control-plane}
    \caption{Chaining-Box control plane}
    \label{fig:control-plane}
\end{figure}

Besides the processing stages discussed in Section
\ref{sec:dataplane}, each \boxname~also has an agent that is
responsible for all administrative tasks related to \archname~on
the execution unit. It handles communication with the controller
and installs and configures the processing stages.

All agent-controller communication is based on a fairly simple
southbound protocol, consisting on two types of messages:
\texttt{Hello} and \texttt{Install}. As the names suggest, the
first one is used by a \boxname~agent to notify the controller of
its existence and its availability to participate in chains,
while the second is used by the controller to send forwarding
rules to each \boxname~instance, which are later installed by the
agent on the Fwd stage. A third type of message to uninstall
rules would also be required to fully support real-world
deployments, but is left as future work.

The job of the \archname~controller consists on:
\begin{enumerate}
    \item \textbf{Parsing chaining configuration}: the
    composition of each chain is specified through a JSON file
    sent to the controller. It parses this file and generates the
    forwarding rules which need to be installed on the
    \boxnameplural~to realize the desired configuration. An
    example of such file is given in Section
    \ref{sec:control-implementation};

    \item \textbf{Establishing connections with \boxnameplural}:
    on startup, every agent sends a \texttt{Hello} message to the
    controller using a TCP connection, which will be kept alive
    as long as the agent and the controller are running, so the
    controller knows which \boxname~are still functional;

    \item \textbf{Installing rules on each \boxname}: after a
    connection is established, if the controller has any chaining
    configuration addressing an specific \boxname, its
    corresponding forwarding rules are sent through an
    \texttt{Install} message.
\end{enumerate}

\section{Architecture} \label{sec:architecture}

The architecture description provided until now in this chapter
was only a high-level view of the underlying principle guiding
\archname's design. In practical terms, however, it may not be
obvious how to realize the processing stages decoupled from both
the network and from service functions.

Graph- or match-action-based packet processing as supported by
VPP and OVS, respectively, could easily be used to implement the
processing stages. These approaches would be fully transparent to
functions, but would be coupled to the corresponding virtual
switching technology used. A programmable-hardware-based solution
also suffers from the same limitation, such as using SmartNICs or
FPGAs to implement processing stages, since that would restrict
the architecture to a few specific hardware devices.

An implementation at the service function level, using procedural
programming, for example, to implement and enforce the order of
processing stages and SF is also feasible, but requires changes
to the function's source code, or implementing it entirely using
specialized frameworks as some of the proposals discussed in
Chapter \ref{ch:related}.

Yet another option is to implement the stages as kernel modules,
the alternative offered by Click-based solutions. This can be
abstracted away by both the network and service functions, by
acting completely in kernel space. \archname~follows this same
path, but instead of Click modules, it uses BPF programs, which
are much simpler, have very little overhead, are native to the
Linux kernel, have support from mainstream toolchains and can be
transparently loaded and modified during runtime with little
system disruption.

Figure \ref{fig:cbox-phys-layout} gives an overview of how
\archname~lays out physically in the system. The SFC mechanism is
entirely placed between the SFs and the underlying network
infrastructure, be it virtual or physical. By using BPF programs
in the kernel, \archname~is capable of handling different system
models, be it a container, a virtual machine or a baremetal
server. All the SFC administrative tasks are handled by the
control plane and configured on the stages through the agent
running in user space. Packets are processed on their way through
the network stack requiring no cooperation from network devices.

\begin{figure}[h]
    \includegraphics[width=0.5\textwidth]{./figs/cbox-physical-layout.pdf}
    \centering
    \caption{Chaining-Box physical layout}
    \label{fig:cbox-phys-layout}
\end{figure}

% \red{In summary, \archname answers the requisites listed on the intrudction in the following manner:}

\section{Comparison with other approaches} \label{sec:arch-comparison}

The separation of concerns between service and data planes
provided by \archname~becomes more evident if we put side-by-side
the physical layout of different architectures such as in Figure
\ref{fig:cbox-layout-comparison}. This figure compares
\archname~against two other architectures: a standard one where
the SFC functionality is all provided by a virtual switch, e.g.
OVS, and PhantomSFC. The boxes in purple highlight where the SFC
mechanism is positioned in each architecture. All three examples
implement the same chain, going through \texttt{SF1} and
\texttt{SF2}, in that order.

\archname~and the standard are very similar in terms of how many
hops each packets needs until the end of the chain. The biggest
difference lies on where SFC actions are being executed in the
system. While \archname~confines everything to each VM, the
standard architecture has it all inside the virtual switch,
coupling the chaining mechanism to the underlying infrastructure.

Although PhantomSFC also removes the SFC mechanism entirely from
the network, this comes at a high cost in terms of performance,
since the Service Function Forwarder (SFF) is fully virtualized
and becomes the center of a star topology with the other SFs.
This way packets have to go back and forth to the SFF before they
finally exit the chain.

\begin{figure}[h]
    \includegraphics[width=\textwidth]{./figs/cbox-physical-layout-comparison.pdf}
    \centering
    \caption{Comparison between Chaining-Box and other architectures}
    \label{fig:cbox-layout-comparison}
\end{figure}

\archname~is able to combine the benefits of both approaches
while avoiding their pitfalls. The following chapters discuss in
great detail how each component of the architecture is
implemented, and the extra benefits added by the use of BPF
technology in the dataplane.