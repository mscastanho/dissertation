\chapter{Prototype Implementation} \label{ch:prototype}

To implement the processing stages discussed in the previous
chapter in a fully transparent manner, \archname~uses BPF
programs inside the Linux kernel. The following sections discuss
the encapsulation types used (\S\ref{sec:additional-headers}),
which BPF hooks were used and how each stage was implemented
(\S\ref{sec:stages-layers}), how \archname~supports different
application types (\S\ref{sec:app-type-support}) and environments
(\S\ref{sec:env-type-support}, possible optimizations when using
containers (\S\ref{sec:direct-links}), and how the control plane
controller and agent were implemented (\S\ref{sec:control-implementation}).

\section{Additional headers}
\label{sec:additional-headers}

As stated before, \archname~makes use of packet encapsulation to
provide SFC. Packets traveling between service functions are
encapsulated with both an external transport encapsulation as
well as with the NSH. In the prototype built the transport
encapsulation corresponds to an additional Ethernet header. For
this reason all \boxname~used in the tests are required to be in
the same L2 network. The decision to use this type of
encapsulation aimed to simplify the prototype, without loss of
generality. Other encapsulation schemes such as VXLAN or Geneve
could also have been used, but are left as future work. Simple
changes to Dec and Enc changes would be sufficient to handle
different encapsulation protocols.

Currently the stages are only capable of handling MD-Type 1 NSH
headers, which have four 4-Byte metadata fields. However, for the
purposes of this text, these fields don't have any special
meaning and were just filled with zeroes. Added to the extra
Ethernet header, the NSH sums up to 38 extra bytes of overhead
for each packet.

\section{Stages and layers} \label{sec:stages-layers}

The main BPF feature leveraged by \archname~is the ability to
process ingress and egress packets on their way through the
network stack, without needing any cooperation or knowledge from
the applications running in the system. Combined with the use of
the NSH, this principle allows this implementation to be
transparent to both the network and service functions. The
following subsections describe the inner workings of each stage.

\subsection{Decapsulation (Dec)}

Since the stages perform header-changing operations on the
packets, ideally they should be executed as close to the NIC as
possible, to have complete power over ingress packets seen by the
kernel and egress ones seen by the network. On the RX side, this
lowest point is represented by the XDP layer, which in its native
form is implemented directly by the device driver. At this point,
the OS has neither parsed incoming packet metadata nor allocated
socket buffers for it, allowing XDP programs to make arbitrary
changes to the packets without the OS' knowledge. For these
reasons, this was the layer chosen for the Dec stage, as shown in
Figure \ref{fig:estagios-no-kernel}.

\begin{figure}[h]
    \includegraphics[width=0.5\textwidth]{./figs/camadas-e-estagios-EN.pdf}
    \centering
    \caption{Stages implemented as BPF programs inside the Linux kernel}
    \label{fig:estagios-no-kernel}
\end{figure}


The Dec stage is implemented in less than 100 lines of C code and
can be summarized to the following actions, illustrated by the
flow diagram of Figure \ref{fig:decap-flow-diag}: (1) detect if
this is the destination of the current Ethernet frame; (2) check
if the packet received contains an NSH header; (3) if so, parse
it's IP 5-tuple; (4) use it as key to save the NSH to a temporary
table implemented as a BPF hash map shared with the Enc stage;
and (5) remove the transport and NSH encapsulations. After all
these steps are complete, the packet is sent up the kernel stack
for further processing. Packets that do not contain the expected
transport and NSH encapsulations are just ignored and passed
along, so the stage does not interfere with other programs and
services running on the same machine.

\begin{figure}[h]
    \includegraphics[width=0.7\textwidth]{./figs/decap-flow-diag.pdf}
    \centering
    \caption{Flow diagram of Dec stage}
    \label{fig:decap-flow-diag}
\end{figure}

When saving NSH information to the map (proxy table) the key
associated with this value is the internal packet's 5-tuple,
ignoring the outer encapsulation, as shown in Figure
\ref{fig:proxy-table}. This same key is used later by the Enc
stage to match outgoing packets for re-insertion of the NSH. Of
course, this approach has limited support for SFs that change the
packet's 5-tuple such as NAT or VPN endpoints. More complex and
clever matching algorithms can be implemented as well, requiring
just implementing new Dec and Enc stages, which can be loaded and
replaced atomically by the BPF system.

\begin{figure}[h]
    \includegraphics[width=0.7\textwidth]{./figs/cbox-proxy-table.pdf}
    \centering
    \caption{Proxy table shared between Dec and Enc stages.}
    \label{fig:proxy-table}
\end{figure}

\subsection{Encapsulation (Enc)}

The initial envisioned use case for the XDP layer was to provide
a faster way to make early decision about incoming packets to
improve the performance of firewall and DDoS mitigation services,
for example. For this reason, this layer is only present on RX,
and not on TX, rendering it unable to implement Enc and Fwd
stages. Although there seems to be plans by the community to
extend the XDP to egress as well, it has not been a priority at
the moment, needing more compelling use cases and killer
applications to steer its development. Thus the last two stages
are implemented on TC, which is the lowest layer on TX side. The
context seen by programs in this layer is also a Layer-2 packet,
allowing fairly generic changes to the packet as well.

The Enc stage receives the packet after it has been processed by
the service function. It's actions are summarized by the flow
diagram in Figure \ref{fig:encap-flow-diag}. It prepares the
packet for the Fwd stage by (1) parsing its 5-tuple; (2) using
that as a key to the BPF hash map shared with the Dec stage to
retrieve the previously removed NSH header; (3) checking whether
this was the last hop in the chain; (4) re-inserting the NSH and
transport encapsulations on the packet; (5) decrementing the SI
value from the NSH; (6) sending it to the next stage. In case the
pre-lookup of the forward table in step (3) shows that this is
the last node in the chain the NSH re-insertion step is skipped
and the packet is sent directly to the network. The check
consists on checking the flags field stored alongside the next
hop address. If this field is set to 1, that indicates the end of
the chain.
% Note that although maps are shared between, there is always
% only one writer.

\begin{figure}[h]
    \includegraphics[width=0.9\textwidth]{./figs/encap-flow-diag.pdf}
    \centering
    \caption{Flow diagram of Enc stage.}
    \label{fig:encap-flow-diag}
\end{figure}

The TC layer has support for internal chains of filters and
actions (not to be confused with SF chains), that are executed in
order defined by a priority value associated with each one. Enc
and Fwd stages are implemented as filters in the same chain, with
Enc having a higher priority than Fwd, and thus executing first.
Similar to the Dec stage, packets that do not match a 5-tuple
stored in the table are deemed unrelated to the SFC chaining, and
are just passed along the stack normally, causing no interference
with other services.

If the packet is successfully re-encapsulated, Enc returns a
\texttt{TC\_ACT\_UNSPEC} value, instructing TC to execute the
next filter in its internal chain, which is configured by
\archname~to be the Fwd stage. This setup, however, suffers
performance penalties as these two disjoint programs will have to
perform packet header parsing separately. This design was chosen
to decouple both stages and make it easier to replace each one
separately as needed, in order to provide different
functionality, for example. However, combining both stages into
one is a simple matter of writing a BPF program that does the job
of both.

\subsection{Forwarding (Fwd)}

\begin{figure}[h]
    \includegraphics[width=0.6\textwidth]{./figs/cbox-forward-table.pdf}
    \centering
    \caption{Lookup table used by Fwd stage to get next hop information.}
    \label{fig:forward-table}
\end{figure}

Finally, the Fwd stage receives packets already encapsulated with
the NSH and uses SPI and SI values from it to lookup the internal
forwarding table (Figure \ref{fig:forward-table}), populated by
the control plane agent. This table is also implemented as a BPF
hash map and uses the SPH (SPI + SI) as the key to retrieve the
address of the next hop. This address is then written to the
packet's transport encapsulation and it is sent back to the
network to be forwarded to the next SF in the chain. All these
actions are summarized by the flow diagram in Figure
\ref{fig:fwd-flow-diag}. Note that packets not containing a
corresponding entry in the forwarding table are dropped, this
situation indicates bogus NSH-encapsulated packets received by
the Dec stage or a misconfigured Fwd stage. In either case, this
packet should not go back to the network, so it is dropped.

\begin{figure}[h]
    \includegraphics[width=0.6\textwidth]{./figs/fwd-flow-diag.pdf}
    \centering
    \caption{Flow diagram of Fwd stage.}
    \label{fig:fwd-flow-diag}
\end{figure}

\section{Application support} \label{sec:app-type-support}

The careful reader may have noticed the discussion of the SF
stage was skipped in the last section. This was intentional as it
is not directly implemented by \archname~itself, but rather by
other existing service function applications. The architecture
supports two kinds of applications discussed in Section
\ref{sec:sfc-app-types}: kernel-supported and kernel-only. Figure
\ref{fig:cbox-app-types} illustrates how the stages interact with
each application type.

\begin{figure}[h]
    \includegraphics[width=\textwidth]{./figs/cbox-app-types.pdf}
    \centering
    \caption{ \archname~supports different kinds of service functions}
    \label{fig:cbox-app-types}
\end{figure}

Kernel-supported applications reside entirely in user space, and
represent the simplest case for \archname. As the stages are
implemented as BPF programs loaded to the kernel, all operations
necessary to steer packets through chains is done without any
interaction from the application, making it totally agnostic to
the SFC environment. Packets are processed and modified by the
stages on their way through the network stack.

Kernel-only functions, on the other hand, can be other BPF
programs also loaded to the kernel, or implemented by
\textit{iptables} rules or kernel modules. For KO apps located
above the TC layer, there is little difference from the KS case,
as \archname~stages will behave as an middle-man to the network
and no special treatment is needed by the architecture. The same
is true for functions implemented on the ingress TC direction, as
illustrated by \ref{fig:cbox-app-types} in the Kernel-only case.
On TC-egress, however, a small change on how the stages are
attached is necessary. The SF must be placed in the same TC chain
as Enc and Fwd stages, but with a higher priority, so it is
executed first. Also, it would require a small change to it's
source code to instruct TC to keep executing the next filters,
i.e. it should return \texttt{TC\_ACT\_UNSPEC} after successfully
processing a packet.

SFs implemented as XDP programs represent a tricky case not fully
supported by \archname~at this moment. Since XDP does not have an
internal chaining mechanism like TC, it is currently not possible
to load multiple XDP programs to the same interface
simultaneously. Although there has been recent proposals for such
mechanism\footnote{https://www.spinics.net/lists/netdev/msg602065.html},
it is still under development and it is not certain whether it
will ever reach the upstream kernel. One possible workaround is
to execute the XDP application as a BPF tail call after Dec stage
execution, but has not been tested yet with \archname.

Lastly, kernel-bypass functions are not supported by \archname.
Since these applications completely bypass the kernel stack, the
processing stages cannot be executed. \texttt{AF\_XDP} sockets
represent a partial solution for this problem, supporting the
execution of XDP programs prior to sending packets directly to
user space, bypassing upper layers in the networking stack.
Application built with DPDK, for example, could use the provided
\texttt{AF\_XDP} poll mode driver (PMD) to use this socket
family, which would permit the execution of the Dec stage. But
since XDP is present only on RX, the egress stages Enc and Fwd
are not contemplated by this solution. Table
\ref{tbl:app-support} summarizes the support offered by
\archname~to each application type.

\begin{table}[]
    \centering
    \begin{tabular}{|c|c|c|c|}
        \hline
        Type             & Dec?    & Enc?    & Fwd?    \\ \hline
        Kernel-supported & Yes     & Yes     & Yes     \\ \hline
        Kernel-only      & Partial & Partial & Partial \\ \hline
        Kernel-bypass    & Yes     & No      & No      \\ \hline
    \end{tabular}
    \caption{Summary of the \archname~stages supported for each SF implementation type.}
    \label{tbl:app-support}
\end{table}
\section{Environment support} \label{sec:env-type-support}

In this section, the word 'environment' refers to the overall
operating system environment where the service function is being
executed, which can be: (1) baremetal; (2) virtual machine; or
(3) container.

When it comes to how \archname~is laid out in the system, the
first two environments are the same, both have a full isolated
kernel that can host the BPF processing stages. Although
containers share the same kernel with the host and other
siblings, each has its own network interfaces, usually one end of
a veth pair or even a passthrough physical interface. Since BPF
programs on both XDP and TC are attached to an specific
interface, these programs are isolated from one another. From
\archname's point-of-view, they behave the same as if each
container had a dedicated kernel stack, enabling the architecture
to work without requiring any changes.

However, there is a possible optimization for the container case.
If consecutive containers in a chain belong to the same host,
they can be interconnected directly through a veth pair, called a
direct link, instead of having to go through a virtual switch.
This brings three main performance benefits: (1) veth pairs can
achieve higher throughput rates than going through a virtual
switch; (2) NSH and transport encapsulations are not necessary,
as this is a direct link; and (3) as a consequence of the lack of
encapsulation, stages Enc and Dec from the source and destination
containers, respectively, can be bypassed. More details about
this optimization are discussed in the next section, while the
overall performance improvement observed is demonstrated in
Section \ref{sec:vethopt}.

\section{Containers and direct links} \label{sec:direct-links}

Figure \ref{fig:cbox-containers} better illustrates how
\archname~can use direct links. On that scenario, \texttt{SF1},
\texttt{SF2} and \texttt{SF3} are part of the same chain and are
executed in sequence. Each is hosted by a separate container, and
all three containers belong to the same host. In this case,
\archname~can instruct the agent to use veth pairs between each
consecutive SF to speedup packet steering through the chain. Fox
example, the \boxname~hosted in Container 1 can have its Enc and
Fwd stages disabled, and forward packets directly through the
veth pair to the next hop in the chain. \texttt{SF2} also has its
Dec stage disabled, since no encapsulation was used by
\texttt{SF1}. Since \texttt{SF2} and \texttt{SF3} are also
connected through a veth pair, Enc and Fwd from \texttt{SF2} and
Dec from \texttt{SF3} are also disabled. In this example, through
the use of 2 veth pairs, 5 stages were omitted, improving the
end-to-end chain latency as will be shown later in this text.

\begin{figure}[h]
    \includegraphics[width=\textwidth]{./figs/cbox-containers.pdf}
    \centering
    \caption{Example of use of veth pairs to simplify packet steering between SFs in containers in the same host}
    \label{fig:cbox-containers}
\end{figure}

\begin{figure}[h]
    \centering
    \subfloat[][Case 1: Two directly linked functions followed by a remote node]{\label{fig:veth-cornercase-middle} \includegraphics[width=0.98\textwidth]{./figs/cbox-veth-corner-case-middle.pdf}}
    \qquad
    \subfloat[][Case 2: Two local nodes, directly linked ending a chain]{\label{fig:veth-cornercase-end} \includegraphics[width=0.8\textwidth]{./figs/cbox-veth-corner-case-end.pdf}}

    \caption{Example of corner cases of direct link optimization that require special treatment by the controller.}
    \label{fig:arq-comp}
\end{figure}

This optimization, however, introduces two corner cases that must
be handled by the controller to keep the chaining functioning
correctly. They happen when the Dec stage is omitted but Enc
still has to be run. The first case is when two \boxname es that
are directly linked are followed by a another without direct link
(Figures \ref{fig:veth-cornercase-middle}). In this case, the
second node will have its Dec stage omitted because of the direct
link connecting it to the previous function, but will still have
the Enc stage as it is needed to communicate with the following
node. The second case happen when when two directly-linked nodes
terminate a chain (\ref{fig:veth-cornercase-end}). Since the last
element in the chain will always have to decapsulate the packet,
it's Enc stage cannot me omitted, but will omit Dec if packets
come from a direct link. In such cases, Enc will lookup the table
for the corresponding NSH header for the packet but no entries
will be found, since no packets were decapsulated and Dec was
never executed.

However, these situations can be detected early by the controller
when configuring the chains, since it knows chain layouts and
which nodes will be configured with direct links. When one the
corner cases are detected, the controller simply adds artificial
entries to the proxy table to Enc stage of the faulty node, as if
they were generated by a packet decapsulation on Dec. This has
the obvious limitation of not permitting that NSH metadata be
carried through the chain, since the controller is unable to
predict the contents of each metadata header beforehand.

\begin{figure}[h]
    \includegraphics[width=0.9\textwidth]{./figs/cbox-tables-filled-json.pdf}
    \centering
    \caption{Example of JSON configuration file given to controller and the corresponding rules generated.}
    \label{fig:tables-example}
\end{figure}

\section{Agent and controller implementation} \label{sec:control-implementation}

Both the controller and agent are implemented in Golang. All code
to interact with the BPF system is written in C using
\textit{libbpf} and \textit{iproute2}, and called from within the
Golang agent. The agents communicate with the remote controller
using TCP connections, that are kept alive as long as the agent
is running. The controller keeps an internal table of all
connected agents, which is used during chain configuration.

A system administrator can specify the chains to be used through
a simple JSON file. This file is parsed by the controller and the
generated rules are sent to the corresponding agents through
their TCP connections to be installed by the agent on each
\boxname. Figure \ref{fig:tables-example} shows and example of
JSON file to configure an environment with two chains. Each chain
is configured with 2 functions each. Each function must have a
tag by which it will be identified in when specifying the chains,
besides the host in which it will be deployed. The value
\texttt{type} was used to indicate which of the implemented test
functions the controller should deploy, but should be changed to
a more generic way to specify any kind of function.

For this input file, the controller will generate one forwarding
rule for each of \texttt{sf1} and \texttt{sf2}, and two for
\texttt{sf3}, as shown on the right side of Figure
\ref{fig:tables-example}. The MAC address of the input interface
of each SF is sent to the controller when the agent sends a Hello
message, and these values are later used to configure the
forwarding tables accordingly. Since both chains end in
\texttt{sf3}, its entries have the MAC address zeroed out and the
flags field is set to 1, indicating end of the chain.

It is also worth noting that the control plane automatically
determines when direct links can be used in chains. This
information can be derived from the \texttt{host} fields from
consecutive functions in a chain. If two SFs are hosted by the
same server and are executed in sequence in a chain, the control
plane automatically connects them with a direct link. On a
full-fledged NFV environment, the JSON configuration file would
represent the output of a placement algorithm, while the controller
described here would carry on the execution and deployment of such
placement.

Lastly, the choice of JSON as the format to define the chaining
configuration was due to its simplicity and builtin support in
many languages, such as Golang. An additional benefit is that
files can be easily validated using
JSONSchema\footnote{https://json-schema.org/}. The support for
other NFV-specific formats such as TOSCA \cite{Garay2016} is
desirable for better integration with existing NFV frameworks but
is left as future work.