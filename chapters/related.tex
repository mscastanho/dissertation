\chapter{Related work} \label{ch:related}

% \vspace{-0.5em} % Without this Latex is placing a huge vspace
% between this paragraph and the two headings

This chapter presents a discussion about how \archname~relates to
other existing SFC mechanisms. In summary, decoupling service and
data planes has not been a primary objective until now. And even
when it has, the final implementations fall short on this
feature. Here \archname~is compared to early SDN-
(\S\ref{sec:sdn-based}), platform- (\S\ref{sec:platform-based}),
and NSH-based approaches (\S\ref{sec:nsh-based}), as well as
others based on segment routing (\S\ref{sec:segment-routing}).
Table \ref{tbl:related-comparison} presents a summary of the
differences between \archname~and the other proposals discussed.
Finally, other projects leveraging BPF in different ways are
present to show how this technology is being used today
(\S\ref{sec:bpf-projects}).

\section{SDN-based} \label{sec:sdn-based}

Most of the initial SFC solutions relied on Software Defined
Networking (SDN) to steer packets through chains
\cite{Medhat2017}. For example, SIMPLE \cite{Qazi2013a} used a
combination of SDN rules and tags to keep track of the current
state of execution of a chain. Middleboxes were treated as
blackboxes and all SFC functionality was implemented by network
devices. Flowtags \cite{Fayazbakhsh2014}, on the other hand,
modified middleboxes to add context-related tags, which in turn
were used by switches to steer packets. Both solutions are
heavily coupled to the network, as they rely on SDN-enabled
switches to operate on specific tags in order to realize the
chains.

\section{Platform-based} \label{sec:platform-based}

Another set of works propose complete platforms for NFV
development and execution, including function interconnection.
These cover a wider scope than \archname, but are mentioned here
as they provide innovative ways to implement chaining
functionality.

NetBricks~\cite{panda2016netbricks} provides a
framework for NFV which is capable of running functions
implemented using a custom language. As all functions reside in
the same execution environment, chaining is provided in terms of
function calls between SFs, leading to low overhead. A somewhat
similar approach is taken by OpenNetVM \cite{Zhang2016}, an NFV
platform that creates an abstraction layer on top of DPDK,
through which network applications can be quickly developed using
the provided constructs. It uses an internal shared memory
mechanism to move packets between functions at low cost. Due to
their holistic approach, these proposals rely on internal
infrastructure provided by the surrounding platforms, and are not
suitable as generic, function-agnostic architectures.

Polycube~\cite{miano2019} is another software framework that
allows the creation of generic service functions, this time based
on BPF. Several helpers are provided to facilitate the
development of services, which are split in a fast path running
as BPF programs inside the kernel an a slow path in user space
for management tasks. Just as \archname, it also benefits from the
advantages offered by BPF technology such as low overhead and the
ability to load programs on demand, but like the other frameworks
discussed requires the re-implementation of functions for this
environment.

% Add discussion about NSH-based architectures
The chaining mechanism used by OpenStack \cite{Openstack2020} is
also worth noting. OpenStack is a modular software that aims to
provide management for computing, storage and networking
resources across a pool of individual servers. It is widely used
to build private clouds and provide Infrastructure as a Service
(IaaS). It is implemented in a distributed manner, being composed
by several separate modules acting together, each providing
different types of services.

Neutron is the module responsible for networking. It manages the
creation of networks, routes packets between VMs, handles access
to the external world, takes care of access control, etc. Every
computing instance is attached to a virtual network through
Neutron ports. This provides a convenient way to handle service
function chaining as a service path is expressed by a list of
pairs of ingress and egress Neutron ports. Service paths also
have flow classifiers, which decide what packets should be
handled by each corresponding path. Neutron uses this information
to send packets from egress to ingress ports, following the
sequence in the port pair list, thus providing service function
chaining. Since this approach heavily relies on Neutron's inner
constructs, it is limited to the OpenStack platform.

\section{NSH-based} \label{sec:nsh-based}

Some implementations also rely on the NSH protocol to implement
SFC. Standalone Open vSwitch (OVS) \cite{OVS2020} provides support for
encapsulation, decapsulation and forwarding based on NSH using
extensions to the OpenFlow standard. SFC can be fully implemented
using OVS as the virtual switch to connect service functions.

OpenDaylight \cite{OpenDaylight2020} features among the most
widely used SDN controllers nowadays. Its SFC implementation is
based on RFC 7665 and uses NSH as the SFC encapsulation of
choice. SFC element functionality is integrated into other
components, instead of being standalone entities. Classifiers are
implemented in two ways: using OpenFlow switches such as OVS or
using ip(6)tables with NetFilterQueue. During chain creation
rules are added to OVS switches that will act as Forwarders and
steer packets through the network functions. SFC Proxies are
treated as abstract entities, capable of being implemented as an
OVS switch, a VNF or even a physical network function.

Fast Data (FD.io) \cite{FDio2020} is a Linux Foundation's project
whose objective is to build an universal data plane, aiming for
speed, efficiency, flexibility and scalability. It is a
collection of projects and libraries to support and improve
software-based packet processing. One of its core projects is the
Vector Packet Processing (VPP) library which was open sourced by
Cisco. This library enables packet processing using a graph
abstraction, allowing developers to create new graph nodes and
users to compose processing graphs built to their needs. NSH SFC
is one the projects under FD.io's umbrella, it seeks to create
the mechanisms necessary to support service chaining using NSH on
VPP. The implementation is also based on RFC 7665 and  NSH, and
implements each SFC element as VPP graph nodes.

These projects are also coupled to the network as their
implementation of SFC logical elements resides in network devices
used by the network, thus requires infrastructure support.
\archname~is capable of fully replacing these complex mechanisms
when it comes to providing SFC, enabling the same functionality
even on top of simpler environments, e.g. based on Linux bridges
and others that do not natively support SFC.

Previous work by the author also aimed to decouple service and
data planes \cite{Castanho2018}. PhantomSFC turns each
RFC 7665 element into separate service functions, instead of
logical roles played by network devices. These special SFs run
alongside other functions, but have the sole objective of
offering services needed to enable SFC (classification, proxying
and forwarding). As every packet needs to go through a Forwarder
at each step of a chain, the logical topology has several
star-like clusters centered on Forwarder SFs, which become
significant points of failure and performance bottlenecks. The
same problem applies to Proxies, at a lesser degree.
\archname~solves these problems by splitting SFC operations into
all SFs, removing the star topology and the bottleneck of
separate Forwarders.

\begin{table}[t]
    \centering
    \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{Name}   & \textbf{Type} & \textbf{\begin{tabular}[c]{@{}c@{}}Unchanged\\ functions?\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Platform-\\ agnostic?\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Network-\\ agnostic?\end{tabular}} & \textbf{\begin{tabular}[c]{@{}c@{}}Generic logical\\  topology?\end{tabular}} \\ \hline
    SIMPLE          & SDN           & Yes                                                                     & No                                                                     & No                                                                    & Yes                                                                           \\ \hline
    FlowTags        & SDN           & No                                                                      & No                                                                     & No                                                                    & Yes                                                                           \\ \hline
    NetBricks       & Platform      & No                                                                      & No                                                                     & Yes                                                                   & Yes                                                                           \\ \hline
    OpenNetVM       & Platform      & No                                                                      & No                                                                     & Yes                                                                   & Yes                                                                           \\ \hline
    Polycube        & Platform      & No                                                                      & Yes                                                                    & Yes                                                                   & Yes                                                                           \\ \hline
    OpenStack       & Platform      & Yes                                                                     & No                                                                     & Yes                                                                   & Yes                                                                           \\ \hline
    Open vSwitch    & NSH           & Yes                                                                     & Yes                                                                    & No                                                                    & Yes                                                                           \\ \hline
    OpenDaylight    & NSH           & Yes                                                                     & No                                                                     & No                                                                    & Yes                                                                           \\ \hline
    VPP             & NSH           & Yes                                                                     & Yes                                                                    & No                                                                    & Yes                                                                           \\ \hline
    PhantomSFC      & NSH           & Yes                                                                     & Yes                                                                    & Yes                                                                   & No                                                                            \\ \hline
    Segment Routing & SR            & No                                                                      & No                                                                     & No                                                                    & Yes                                                                           \\ \hline
    Chaining-Box    & NSH           & Yes                                                                     & Yes                                                                    & Yes                                                                   & Yes                                                                           \\ \hline
    \end{tabular}
    \caption{Comparison between related SFC mechanisms and \archname}
    \label{tbl:related-comparison}
\end{table}

\section{Segment Routing} \label{sec:segment-routing}

% Following on that same note
A new technique that has been used recently to enable SFC is
segment routing (SR) using IPv6
\cite{Abdelsalam2017,Duchene:2018:EVU:3234200.3234213}. In this
approach, each packet is encapsulated with a Segment Routing
Header (SRH) \cite{I-D.ietf-6man-segment-routing-header}, an
extension to the IPv6 protocol, containing a stack of IPv6
addresses of nodes in a chain, called segments. At each hop,
SR-enabled network devices execute their correspondent service
functions and pop the top-most address from the stack, rewriting
the destination IPv6 address to the next segment in the chain.
Intermediate SR-unaware devices simply operate on the regular
IPv6 fields to route the packet normally.

In this approach, the entire sequence of instances to operate
over the packet is rendered by an initial classifier, simplifying
the intermediate forwarding elements. On the other hand, it can
only be used with IPv6 or MPLS protocols, limiting its scope. In
addition, the extra packet size needed to specify the list of
addresses can be big for long chains, increasing packet size and
overall header overhead. Xhonneaux \textit{et al.}
\cite{Xhonneux:2018:LEP:3281411.3281426} propose an SFC scheme
using BPF programs in the Linux kernel stack to implement segment
routing. It resembles closely the approach taken by \archname,
with the difference being how the the underlying SFC technique
used.

\section{Projects leveraging BPF} \label{sec:bpf-projects}

Just like BPF is the key technology behind \archname, it has also
enabled the development of many other relevant projects in recent
years. InKev~\cite{Ahmed:2018:IID:3243157.3243161} enables to
execute BPF programs on the datapath for virtual networks,
targeting data center networks. Tu \textit{et
al.}~\cite{Tu:2017:BEO:3139645.3139657} describe the design,
implementation and evaluation of a BPF-based extensible datapath
for OVS. To enable OpenFlow to parse arbitrary field, Jouet
\textit{et al.}~\cite{Jouet2015OpenFlow} defined an OpenFlow
Extended match filed (OXM) to install cBPF bytecode and added a
libpcap engine to Openflow software switch to execute
it~\cite{Jouet2015OpenFlow}. Xhonneux \textit{et al.}
\cite{Xhonneux:2018:LEP:3281411.3281426} utilizes BPF to provide
a programmable interface for IPv6 Segment routing, as discussed
previously.

BPF is also being used in production systems. Examples include
Cloudflare, which uses programs in multiple hooks in its network
stack to implement DDoS mitigation, load balancing, and socket
filtering and dispatching \cite{cloudflare-usecases}. Facebook
implemented and open-sourced an L4 load balancer based on XDP,
called Katran~\cite{Katran2018}. Beyond the networking field, BPF
has also proven an invaluable tool for tracing, the reason why it
is being used by Netflix for performance monitoring and system
profiling~\cite{netflix}.

Moreover, the Cilium \cite{cilium2019} open-source project uses
BPF extensively to provide networking and security for
microservice applications, being aware of API-level details
beyond simple network headers. Weave Scope leverages BPF to track
TCP connections on Kubernetes clusters \cite{weavescope-ebpf},
and Project Calico has also announced recenty that a new data
plane for container networking based on eBPF is being developed
\cite{calico-ebpf}.