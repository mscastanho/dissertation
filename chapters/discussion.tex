\chapter{Discussion} \label{ch:discussion}

In face of the architectural design and the experimental
evaluation discussed before, this chapter discusses some
of the advantages (\S\ref{sec:advantages}) and disadvantages
(\S\ref{sec:disadvantages} of \archname~compared to other
architectures. Afterwards, challenges and limitations of
current technology faced while developing \archname~are presented
(\S\ref{sec:challenges}).

\section{Advantages} \label{sec:advantages}

The advantages of \archname~can be split in two categories: the
ones derived directly from the architectural concept proposed (\S\ref{ssec:adv-arch}) and others that stem from the choice
of using BPF as the technology to implement the processing stages
(\S\ref{ssec:adv-bpf}). These are discussed in the following
sections.

\subsection{Benefits offered by the architecture} \label{ssec:adv-arch}


\textbf{Transparency to both SFs and underlay network}: by moving
all SFC functionality to the SF's executing unit, the need
for NSH support by the network infrastructure and the
service function are lifted. In case the function is NSH-aware,
this transparency is not needed, so both Enc and Dec stages can
be disabled, improving function performance. However, the
facilities offered by \archname's processing stages allow legacy
functions, i.e. NSH-unaware, to be part of a chain without any
changes to its source code.

\textbf{Flexible support for different proxy operations}: in the
architecture proposed by RFC 7665, packet matching done by the
proxies is not a trivial task. These elements need to match
incoming and outgoing packets to re-insert the SFC encapsulation,
or perform re-classification to add a new NSH header. However,
the architecture does not impose any kind of restriction on the
kind of changes on the packet allowed to service functions. These
can go from header and payload modification to insertion and
deletion of information and even creation of extra new packets.
This generic nature turns packet matching into a challenging
task, often requiring complex algorithms \cite{Qazi2013a}, that
although functional are not fully precise. By moving these
actions to the SF's execution unit's kernel, for example, they
are executed on the same software domain, i.e., the same
operating system.

Although beyond the scope of this work, in this setting packet
matching could be facilitated by the use of structures offered by
the OS itself. For example, if a zero-copy mode for packet
handling is employed, matching single incoming and outgoing
packets out become trivial, as matching packets would correspond
to the same memory address. Other possibility would be to use
packet metadata created by the kernel to identify packets and
support correspondence between them.

\archname~can be extended to support different types of functions
by implementing function-specific Dec and Enc stages. Since they
can be easily modified and reloaded to the kernel, one could
apply different packet matching techniques based on prior
knowledge on a specific function, i.e. about its behavior. In
this case, different version of Dec and Enc stages would be
loaded depending on what kind of function was being used. These
changes would be limited to the service plane, not requiring
changes to functions, keeping \archname's primary goal of being
transparent.

\textbf{Greater resilience to failure}: as discussed in chapter
\ref{ch:architecture}, since \archname~proposes a better
distribution of SFC actions, there is a reduction on single
points of failure that were concentrated on the Forwarders on the
proposal of RFC 7665. That architecture relied on several
interconnected star-like groups centered on Forwarders, with the
consequence that a failure on one of the central points would
affect all functions connected to it and the chains they belonged
to. \archname~allows the service plane to use more generic
logical topologies, removing the centralized nodes. In this case,
the failure of a single node disturbs only the chains that
particular instance is part of.

\textbf{Smaller forwarding tables}: an extra consequence of each
SF instance having its own dedicated Fwd stage is that the tables
used by it for SFC forwarding will have less entries, as only the
ones concerning the associated instance will be needed. This
contrasts with the forwarding tables used by Forwarders in RFC
7665, which needed entries for several different instances
attached to that particular element.

\subsection{Benefits offered by BPF} \label{ssec:adv-bpf}


\textbf{Programmable stages}: by nature BPF programs can be
installed atomically by the kernel and have their functionality
modified at any moment during run time. Its language is also
generic enough to implement a wide range of programs. This makes
the processing stages highly flexible and programmable. For
example, a different packet matching technique can be implemented
as a new pair of Dec and Enc stages and loaded instead of the
standard programs, without knowledge from network and service
function. The same could be applied to use new SFC or transport
encapsulations or even change how the Fwd stage implements the
forwarding functionality.

\textbf{Offload to programmable devices}: there are devices
capable of executing BPF programs in hardware
\cite{kicinski2016ebpf,pacifico2018roteador}, and also native
driver support offered by Linux to offload programs to them. This
allows even better performance than what is obtained when running
the programs inside the kernel.

\textbf{Low resource usage}: as shown previously, different from
DPDK, which requires dedicated resources to execute at high
speeds, BPF programs on XDP and TC allow similar performance at a
considerable lesser memory and processor footprint.

\section{Disadvantages} \label{sec:disadvantages}

% \textbf{Higher control message overhead}: as the control plane
% has to communicate with all nodes to install rules, instead of
% just the Forwarder nodes as in RFC 7665, there is an increased
% overhead on the number of control messages sent by the control
% plane. Also, each control packet will have a reduced size, as it
% will only contain the forwarding rules related to a single
% \boxname, instead of several.

\textbf{The remaining need for a classifier}: even though the
architecture is transparent to network and SFs, it still relies
on a classifier to add the NSH header to each packet ingressing
the SFC Domain, which will require some cooperation from the
underlying network to either direct all packets to a few hosts
running a software Classifier or implement such functionality on
a border router, for example. Even with this remaining
restriction, \archname~represents a significant improvement
towards a completely infrastructure-independent SFC architecture.

\textbf{Dependence on encapsulation}: the use of an extra
encapsulation for SFC has a performance penalty caused by the
constant encapsulation and decapsulation operations when using
NSH-unaware functions. This may also cause fragmentation issues.
However, this proposal aims for deployment on environments where
an administrator has full power over the entire infrastructure,
such as inside service provider datacenter networks. On such
situations, the provider can configure the MTU of intermediate
devices to a proper value considering the additional
encapsulation, in order to avoid problems with packet
fragmentation.

\textbf{Implementation restricted to Linux}: the proposed
implementation based on BPF has the disadvantage of being limited
to Linux environments, as the recent changes to BPF system are
only available on that platform. The FreeBSD community has
discussed about supporting the improvements offered by
eBPF~\footnote{https://www.bsdcan.org/2018/schedule/track/Hacking/963.en.html},
but at the time of writing this has not been fully implemented
yet. However, this is a minor restriction since most cloud and
datacenter environments are Linux-based, thus able to support
\archname~if a more recent kernel version is used.

\textbf{Lack of support for KB type functions}: as discussed in
\S\ref{sec:app-type-support} \archname~currently does not support
kernel-bypass functions, even if using AF\_XDP sockets. An
alternative approach would be to implement a BPF VM apart from
the kernel, which could be used to create ingress and egress
hooks for DPDK applications, for example, to which the processing
stages could be loaded. Standalone implementations like this
already exist, such as the BPF library on DPDK and the
\textit{ubpf} project \cite{uBPF2019}.

\textbf{Packet matching}: since the architecture is based on an
external encapsulation, it requires matching packets, which is a
difficult task. On the prototype implemented, this is done by
performing an exact match on the packet's 5-tuple. This approach
restricts the range of supported applications to only those that
do not alter the packet's 5-tuple. However, as the stages are
fully-programmable, other techniques can be used to implement Dec
and Enc stages and change such behavior.
\vspace{-1em}

\section{Other issues} \label{sec:challenges}

The following items represent existing pain points caused by the
lack of feature support by existing technologies, and are not
directly caused by \archname's design choices.
%These are barriers that were not overcome during development.

\textbf{No XDP on TX}: the lack of an XDP hook on the egress
direction of network interfaces hinders \archname~ in two ways.
First, it forces Enc and Fwd stages be implemented on TC-egress.
If not properly configured, other TC filters and actions can be
executed after these stages, potentially affecting SFC
functionality. By using an XDP hook, it would be more likely that
these two last processing stages were some of the last pieces of
code operating on the outgoing packet. The second consequence is
that no BPF programs can be executed on egress packets from
AF\_XDP sockets, thus kernel-bypass functions are not supported
by \archname.


\textbf{No chained XDP execution}: currently XDP programs cannot
be executed in sequence, as is supported for TC programs using
priorities and chains, for example. Thus each interface can only
have a single XDP program attached to it. Chained execution can
be emulated by the use of tail calls, but requires some changes
to programs. Thus \archname~currently cannot operate with service
functions running on the XDP layer without changes to their
source code.

\textbf{Blackbox service functions}: some existing service
functions are implemented as black boxes, not allowing changes to
its execution unit (be it VMs or containers). This makes it
impossible to load the BPF processing stages, requiring the use a
different technology for implementing the architecture.