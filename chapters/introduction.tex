\chapter{Introduction}
\
% Contextualization

Many complex network environments rely on the execution of a
sequence of several different applications, also called Service
Functions (SF), to provide a complete service for each incoming
packet. For example, requests may have to go through a firewall
followed by a deep packet inspector (DPI) before accessing a
database server. To fulfill this sequence, packets must be forced
to go through each function before being sent to their final
destination, an operation called Service Function Chaining (SFC).

In the past such functions used to be deployed as hardware
middleboxes, and packets were steered through them using
cumbersome and inflexible network configurations. This approach
posed serious challenges for environments that must quickly
respond to changes in demand, such as cloud and
telecommunications datacenters \cite{rfc7498}. The introduction
of the Network Function Virtualization (NFV) paradigm partially
tackled these problems, migrating network functions from hardware
middleboxes to a software-based model~\cite{Mijumbi2016}.

Beyond the many challenges involved in this transition, new
mechanisms were required to provide SFC for virtualized
functions, beyond what traditional routing and switching could
offer. Many organizations and research groups have worked towards
enabling this feature~\cite{rfc7665, Qazi2013,
Xhonneux:2018:LEP:3281411.3281426}, and many
solutions are already in used production systems.

% Quick overview of existing approaches
Early solutions used Software Defined Networking (SDN) techniques
to provide function chaining
\cite{Zhang2013,Qazi2013,Fayazbakhsh2014}, often relying on
OpenFlow-enabled switches to steer traffic through functions.
With time, more holistic approaches were proposed, like the
creation of high-performance frameworks for the development and
interconnection of SFs \cite{Martins2014, panda2016netbricks,
Zhang2016}. Today, SFC is provided by several mainstream
platforms such as Open vSwitch (OVS) \cite{OVS2020}, VPP
\cite{VPP2020}, OpenStack \cite{Openstack2020} and OpenDaylight
\cite{OpenDaylight2020}.
% The problem
However, all these proposals share the same limitations: they are
dependent on specialized network devices or require functions to
be re-implemented using platform-specific constructs. These cause
current SFC mechanisms to be tailor-made to the platforms or
devices they were built for, and also tightly coupled to the
underlying infrastructure.

% Requirements for SFC architecture Keep the same requisites as
% othes:
% - Allow quick reconfiguration to provider's needs and demand
% - Easy to scale to multiple nodes Desired requisites:
% - No cooperation from service functions
% - No cooperation from the network infrastructure
% - Not tied to an domain-specific technology / generic enough to
%   be deployed on different environments
Ideally, service functions and network infrastructure should be
completely agnostic to any SFC mechanisms in place, allowing
greater flexibility for modifications and reuse by different
deployments, without having to re-invent the wheel. For such, a
fully transparent SFC architecture would require (1) no
knowledge by service functions (2) and network infrastructure,
(3) while being generic enough to be deployed in different
environments, facilitating portability. All this without
abstaining from (4) the ability for quick reconfiguration and (5)
from being easily scalable to multiple instances as current
solutions.
% Problem Statement
Thus the main goal of this work is to answer the question:
\textit{How to provide SFC in a fully transparent manner while
meeting all these requirements?}

% The underlying challenges that need to be overcome Each
% requisite comes with a challenge
% - Should be able to operate "around" the functions
% - Cannot depend on network devices
% - Built upon a common denominator between several platforms, so
%   it is readily available to all
Those items translate directly into a set challenges, namely (1)
how to operate \textit{around} a service function, without its
full cooperation, (2) how to use the network for packet steering
without requiring changes to its operation, and (3) how to build
the SFC mechanism upon a common denominator readily available to
most platforms.


% Our magical solution to this problem It checks all the boxes
% above and provides extra benefits such as quick
% reconfiguration, programmable and hot-pluggable functionality,
% low overhead, available everywhere, just like Linux.
To solve these problems and answer the question above, this work
proposes \archname, a new SFC architecture that fully decouples
service and data planes, allowing functions and network
infrastructure to be agnostic to the SFC functionality. It uses
NSH \cite{rfc8300} to provide an overlay for SFC and condenses
all SFC-related actions into a set of sequential stages executed
as BPF~\cite{miano2018} programs inside the standard Linux
kernel. Each program is compiled to a bytecode and run by the
kernel at several hook points, which are executed as packets flow
through the system's network stack. This approach introduces a
high level of independence between SFs, SFC architecture and the
underlying network, allowing on-demand modifications to the
architecture's components with little to no disruption to SFs.
% and low overhead.

% How the rest of the text is organized
The remainder of this text exposes concepts
(\S\ref{ch:background}), discusses further other solutions and
their shortcomings (\S\ref{ch:related}), presents a detailed view
of the architecture design (\S\ref{ch:architecture}) and the
implementation of a prototype (\S\ref{ch:prototype}). The results
of an experimental evaluation to understand its performance and
feasibility are presented (\S\ref{ch:evaluation}), and
\archname's main benefits and drawbacks are also discussed
(\S\ref{ch:discussion}). The text is concluded with some final
remarks and future work (\S\ref{ch:conclusion}). All the code
developed is available on
GitHub\footnote{https://github.com/mscastanho/chaining-box}.